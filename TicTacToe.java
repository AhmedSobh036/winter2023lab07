import java.util.Scanner;
public class TicTacToe
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome to the Tic Tac Toe game!");
		System.out.println("Player 1's token is X");
		System.out.println("Player 2's token is O");
		Board game1 = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		while(!gameOver)
		{
			System.out.println(game1);
			if(player == 1)
			{
				playerToken = Square.X;
			}
			else
			{
				playerToken = Square.O;
			}
			System.out.println("Player "+player+": Please type in which row you would like to place your token");
			int row = scan.nextInt();
			System.out.println("Player "+player+": Please type in which collumn you would like to place your token");
			int col = scan.nextInt();
			while(!game1.placeToken(row, col, playerToken))
			{
				System.out.println("Player "+player+": Please enter a row between the values of 0 and 2 and that is empty on the grid.");
				row = scan.nextInt();
				System.out.println("Player "+player+": Please enter a collumn between the values of 0 and 2 and that is empty on the grid.");
				col = scan.nextInt();
			}
			if(game1.checkIfWinning(playerToken))
			{
				System.out.println("Player " + player + " is the winner!");
				System.out.println(game1);
				gameOver = true;
			}
			else if(game1.checkIfFull())
			{
				System.out.println("The game ended in a tie!");
				gameOver = true;
			}
			else
			{
				player ++;
				if(player > 2)
				{
					player = 1;
				}
			}
		}
	}
}