public class Board
{
	private Square[][] tictactoeBoard;
	
	public Board()
	{
		tictactoeBoard = new Square[3][3];
		for(int i=0; i < tictactoeBoard.length; i++)
		{
			for(int j=0; j < tictactoeBoard[i].length; j++)
			{
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString()
	{
		String grid = "  0  1  2\n";
		for (int i=0; i < tictactoeBoard.length; i++)
		{
			grid+= i+" ";
			for(int j=0; j < tictactoeBoard[i].length; j++)
			{
				grid+=tictactoeBoard[i][j] + "  ";
			}
			grid += "\n";
		}
		return grid;
	}

	public boolean placeToken(int row, int col, Square playerToken)
	{
		
		if(row > 2 || col > 2 || row < 0 || col < 0)
		{
			return false;
		}
		
		if(tictactoeBoard[row][col] == Square.BLANK)
		{
			tictactoeBoard[row][col] = playerToken;
			return true;
		}
		return false;
	}
	
	public boolean checkIfFull()
	{
		for (int i=0; i < tictactoeBoard.length; i++)
		{
			for(int j=0; j < tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[i][j] == Square.BLANK)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		int count = 0;
		for(int i=0; i < tictactoeBoard.length; i++)
		{
			for(int j=0; j < tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[i][j] == playerToken)
				{
					count++;
				}
			}
			
			if(count == 3)
			{
				return true;
			}
			else
			{
				count = 0;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken)
	{
		int count = 0;
		for(int i=0; i < tictactoeBoard.length; i++)
		{
			for(int j=0; j < tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[j][i] == playerToken)
				{
					count++;
				}
			}
			
			if(count == 3)
			{
				return true;
			}
			else
			{
				count = 0;
			}
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken)
	{
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken))
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}